# Installation
> `npm install --save @types/minipass`

# Summary
This package contains type definitions for minipass (https://github.com/isaacs/minipass#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/minipass.

### Additional Details
 * Last updated: Thu, 13 Jan 2022 20:01:40 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [BendingBender](https://github.com/BendingBender).
